<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Partners;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Price as PriceRegistry;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Prices as PriceCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\Partner as PartnerInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as EntityInterface;
use AdaLiszk\Trivago\Recruiting\Providers\Entity as EntityProvider;
use stdClass;

/**
 * Class PartnerEntity
 *
 * Just a Partner type Entity
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package AdaLiszk\Trivago\Recruiting
 * @subpackage Partners
 * @category Entity
 * @since v1.0.0
 */
class Partner extends EntityProvider implements PartnerInterface
{
    /** @var array of required properties for fill with data */
    protected $fillable = ['id','url','prices'];

    /** @var string */
    protected $url;

    /** @var PriceCollection */
    protected $prices;

    /** @var PriceRegistry */
    protected $priceRegistry;

    /**
     * Get dependencies and initialize
     *
     * @param stdClass|null $dataSet
     * @param PriceRegistry $priceRegistry
     */
    public function __construct($dataSet = null, PriceRegistry $priceRegistry)
    {
        $this->priceRegistry = $priceRegistry;

        if (!empty($dataSet)) $this->initializeProperties($dataSet);
        if (is_object($dataSet) && isset($dataSet->proces))
        {
            $this->prices = null;
            foreach ($dataSet->prices as $priceId)
            {
                if (empty($this->prices)) $this->partners = $priceRegistry->getById($priceId);
                else $this->prices->unionWith($priceRegistry->getById($priceId));
            }
        }
    }

    /**
     * Getter for Address property
     * @return string
     */
    public function url(): string { return $this->url; }

    /**
     *
     * Getter for Partners property
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @return PriceCollection|CollectionInterface
     */
    public function prices(): Collection
    {
        return $this->prices;
    }

    /**
     * Creating a new instance from the same Entity
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @param stdClass $initialData
     * @return PartnerInterface|EntityInterface
     */
    public function newInstance(stdClass $initialData): EntityInterface
    {
        return new self($initialData);
    }
}