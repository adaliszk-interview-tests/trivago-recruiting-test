<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Services;


use AdaLiszk\Trivago\Recruiting\Boundaries\Services\Hotels as HotelService;

use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\City as CityRegistryInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Cities as CityCollection;

use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Hotel as HotelRegistryInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels as HotelCollection;

use InvalidArgumentException;

class Hotels implements HotelService
{
    private $cityRegistry;
    private $hotelRegistry;

    /**
     * Get dependencies and initialize
     *
     * @param CityRegistryInterface $cityRegistry
     * @param HotelRegistryInterface $hotelRegistry
     */
    public function __construct(
        CityRegistryInterface $cityRegistry,
        HotelRegistryInterface $hotelRegistry
    ) {
        $this->cityRegistry = $cityRegistry;
        $this->hotelRegistry = $hotelRegistry;
    }

    /**
     * @param string $cityName Name of the city to search for.
     * @throws InvalidArgumentException if city name is unknown.
     * @return HotelCollection
     */
    public function getByCityName(string $cityName): HotelCollection
    {
        /** @var CityCollection $cities */
        $cities = $this->cityRegistry->getByName($cityName);

        /** @var HotelCollection $hotels */
        $hotels = $this->hotelRegistry->getByCities($cities);

        return $hotels;
    }
}