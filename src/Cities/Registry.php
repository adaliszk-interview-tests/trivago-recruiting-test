<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Cities;

use AdaLiszk\Trivago\Recruiting\Boundaries\FileDataSource as DataSourceInterface;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Cities as CityCollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\City as CityEntityInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;

use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\City as CityRegistryInterface;
use AdaLiszk\Trivago\Recruiting\Providers\Registry as RegistryProvider;

use ReflectionClass;

/**
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 */
class Registry extends RegistryProvider implements CityRegistryInterface
{
    private $cityClass;
    private $cityCollectionClass;

    protected $entities = [];
    protected $names = [];

    /**
     * Get the Dependencies
     *
     * @param DataSourceInterface $dataSource
     * @param CityEntityInterface $cityEntity
     * @param CityCollectionInterface $cityCollection
     * @internal param DataSourceInterface $database
     */
    public function __construct(
        DataSourceInterface $dataSource,
        CityEntityInterface $cityEntity,
        CityCollectionInterface $cityCollection
    ) {
        parent::__construct($dataSource, DATA_PATH . 'cities.json');

        $this->cityCollectionClass = new ReflectionClass($cityCollection);
        $this->cityClass = new ReflectionClass($cityEntity);

        foreach ($this->db->getContent() as $item)
        {
            /** @var CityEntityInterface $city */
            $city = $cityEntity->newInstance($item);

            // Getting property pointers into memory for less cpu work
            $id = $city->id(); $name = $city->name();

            $this->entities[$id] = $city;
            $this->names[$name] =& $this->entities[$id];
        }
    }

    /**
     * Basic query for an Entity
     *
     * @param int $id
     * @return CityCollectionInterface|CollectionInterface
     */
    public function getById(int $id): CollectionInterface
    {
        /** @var CityCollectionInterface $collection */
        $collection = $this->cityCollectionClass->newInstance();

        if (isset($this->entities[$id]))
            $collection->add($this->entities[$id]);

        return $collection;
    }

    /**
     * Returning cities with the expected name
     *
     * @param string $cityName
     * @return CityCollectionInterface
     */
    public function getByName(string $cityName): CityCollectionInterface
    {
        /** @var CityCollectionInterface $collection */
        $collection = $this->cityCollectionClass->newInstance();

        if (isset($this->names[$cityName]))
            $collection->add($this->names[$cityName]);

        return $collection;
    }

    /**
     * Get all Results
     *
     * @return CityCollectionInterface|CollectionInterface
     */
    public function getAll(): CollectionInterface
    {
        /** @var CityCollectionInterface $collection */
        $collection = $this->cityCollectionClass->newInstance();

        foreach ($this->entities as $cityEntity)
            $collection->add($cityEntity);

        return $collection;
    }
}