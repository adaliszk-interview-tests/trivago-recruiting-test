<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Providers;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as ItemInterface;
use SplObjectStorage as CollectionStorageEngine;

/**
 * Just a wrapper around SplObjectStorage so I could use it as abstract
 * sadly it doesn't has proper abstract methods yet.
 */
abstract class Collection extends CollectionStorageEngine implements CollectionInterface
{
    /**
     * Add item to the collection
     *
     * @param ItemInterface $item
     * @param mixed $data
     */
    public function add(ItemInterface $item, $data = NULL): void { $this->attach($item, $data); }

    /**
     * Checks that an Item is already in the collection
     *
     * @param ItemInterface $item
     * @return bool
     */
    public function containing(ItemInterface $item): bool { return $this->contains($item); }

    /**
     * Remove item from the collection
     *
     * @param ItemInterface $item
     */
    public function remove(ItemInterface $item): void { $this->detach($item); }

    /**
     * For change the inner logic of the uniqueHash you have to override the getHash method!
     *
     * @param ItemInterface $item
     * @return string
     */
    public function uniqueHash(ItemInterface $item): string { return $this->getHash($item); }

    /**
     * Make Union with a given collection and the result would be the base Collection having all of the items
     *
     * @param Collection|CollectionInterface $collection
     */
    public function unionWith(CollectionInterface $collection): void { $this->addAll($collection); }

    /**
     * Subtract items from the collections using an other collection
     *
     * @param Collection|CollectionInterface $collection
     */
    public function subtractItems(CollectionInterface $collection): void { $this->removeAll($collection); }

    /**
     * Subtract items from the collection but keeping the given collection items if they are in the base collection
     *
     * @param Collection|CollectionInterface $collection
     */
    public function intersectWith(CollectionInterface $collection): void { $this->removeAllExcept($collection); }

    /**
     * By default the hash is the Entity ID
     *
     * @param ItemInterface $item
     * @return mixed
     */
    public function getHash($item) { return "{$item->id()}"; }
}
