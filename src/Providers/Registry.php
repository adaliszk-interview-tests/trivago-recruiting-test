<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Providers;

use AdaLiszk\Trivago\Recruiting\Boundaries\Registry as RegistryInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\FileDataSource as DataSourceInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collection as CollectionInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as EntityInterface;
use stdClass;

abstract class Registry implements RegistryInterface
{
    /** @var DataSourceInterface */
    protected $db;

    /**
     * Get dependencies and initialize
     *
     * @param DataSourceInterface $dataSource
     * @param string $sourceName
     */
    public function __construct(DataSourceInterface $dataSource, string $sourceName)
    {
        $this->db = $dataSource;
        $this->db->open($sourceName);
    }

    /**
     * Basic query for an Entity
     *
     * @param int $id
     * @return CollectionInterface
     */
    public abstract function getById(int $id): CollectionInterface;

    /**
     * Get all Results
     *
     * @return CollectionInterface
     */
    public abstract function getAll(): CollectionInterface;
}