<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Boundaries;

use Countable;
use Iterator;
use Serializable;
use ArrayAccess;

interface Collection extends Countable, Iterator, Serializable, ArrayAccess
{
    public function add(Entity $item, $data = NULL): void;
    public function containing(Entity $item): bool;
    public function remove(Entity $item): void;

    public function uniqueHash(Entity $item): string;

    public function unionWith(Collection $collection): void;
    public function subtractItems(Collection $collection): void;
    public function intersectWith(Collection $collection): void;
}