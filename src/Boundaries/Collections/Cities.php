<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Boundaries\Collections;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\City as CityEntity;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity;

interface Cities extends Collection
{
    /** @noinspection PhpDocSignatureInspection
     *
     * Add item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param CityEntity $item
     * @param mixed $data
     */
    public function add(Entity $item, $data = NULL): void;

    /** @noinspection PhpDocSignatureInspection
     *
     * Remove item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param CityEntity $item
     */
    public function remove(Entity $item): void;
}