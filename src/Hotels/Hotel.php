<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Hotels;

use AdaLiszk\Trivago\Recruiting\Providers\Entity as EntityProvider;

// Interfaces for dependencies
use AdaLiszk\Trivago\Recruiting\Boundaries\Entities\Hotel as HotelInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Partner as PartnerRegistry;
use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Partners as PartnerCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Entity as EntityInterface;
use AdaLiszk\Trivago\Recruiting\Boundaries\Data as DataInterface;


/**
 * Class HotelEntity
 *
 * Just a Hotel type Entity
 *
 * @author Ádám Liszkai <adaliszk@gmail.com>
 * @package AdaLiszk\Trivago\Recruiting
 * @subpackage Hotels
 * @category Entity
 * @since v1.0.0
 */
class Hotel extends EntityProvider implements HotelInterface
{
    /** @var array of required properties for fill with data */
    protected $fillable = ['id','name','address', 'partners'];

    /** @var string */
    protected $name;

    /** @var string */
    protected $address;

    /** @var PartnerCollection */
    protected $partners;

    /** @var PartnerRegistry */
    protected $partnerRegistry;

    /**
     * Get dependencies and initialize
     *
     * @param DataInterface|null $dataSet NOTE: I cannot set the type in arguments because the dependency resolver will try to fill the gap.
     * @param PartnerRegistry $partnerRegistry
     */
    public function __construct($dataSet = null, PartnerRegistry $partnerRegistry)
    {
        $this->partnerRegistry = $partnerRegistry;

        if (!empty($dataSet))
        {
            $this->initializeProperties($dataSet);

            // NOTE: I don't need to check it's existence because initializeProperties throws Exception in that case
            $this->initializePartners($dataSet['partners']);
        }
    }

    /**
     * Initialize PartnerCollection
     * @param iterable $partners
     */
    private function initializePartners(iterable $partners)
    {
        if (!($partners instanceof PartnerCollection))
        {
            $this->partners = null;

            foreach ($partners as $id)
            {
                if (empty($this->partners)) $this->partners = $this->partnerRegistry->getById($id);
                else $this->partners->unionWith($this->partnerRegistry->getById($id));
            }
        }
    }

    /**
     * Getter for Name property
     * @return string
     */
    public function name(): string { return $this->name; }

    /**
     * Getter for Address property
     * @return string
     */
    public function address(): string { return $this->address; }

    /**
     * Getter for Partners property
     * @return PartnerCollection
     */
    public function partners(): PartnerCollection { return $this->partners; }

    /**
     * Creating a new instance from the same Entity
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @param DataInterface|null $initialData
     * @return HotelInterface|EntityInterface
     */
    public function newInstance(?DataInterface $initialData = null): EntityInterface
    {
        return new self($initialData, $this->partnerRegistry);
    }
}