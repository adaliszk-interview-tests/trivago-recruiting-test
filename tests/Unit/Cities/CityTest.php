<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace AdaLiszk\Trivago\Recruiting\Tests\Unit\Cities;

use AdaLiszk\Trivago\Recruiting\Tests\TestCase;
use ReflectionClass;

use AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Hotels as HotelCollection;
use AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Hotel as HotelRegistry;

class CityTest extends TestCase
{
    /** @var ReflectionClass of the class */
    private $class;

    /** @var ReflectionClass of the interface */
    private $interface;

    /**
     * Initialize ReflectionClasses
     */
    public function setUp()
    {
        parent::setUp();

        $this->class = new ReflectionClass($this->namespaceRoot.'Cities\\City');
        $this->interface = new ReflectionClass($this->namespaceRoot.'Boundaries\\Entities\\City');
    }

    /**
     * Sanity Check: Is the class even implements the desired interface?
     * @test
     */
    public function isTestable()
    {
        $this->assertArrayHasKey(
            $this->interface->getName(),
            $this->class->getInterfaces(),
            "Class {$this->class->getName()} is not implements {$this->interface->getName()}"
        );
    }

    /**
     * Provide multiple type of data for testing
     */
    public function dataSourceProvider()
    {
        $hotelCollection = $this->createMock(HotelCollection::class);

        return [
            'Valid Data -> id()' => [['id'=>1,'name'=>'Alpha', 'hotels'=>[1]], 'id', 1, NULL],
            'Valid Data -> name()' => [['id'=>2,'name'=>'Bravo', 'hotels'=>[2]], 'name', 'Bravo', NULL],
            'Valid Data -> hotels() / one' => [['id'=>3,'name'=>'Charlie', 'hotels'=>[3]], 'hotels', $this->createCollectionMock($hotelCollection, [3]), NULL],
            'Valid Data -> hotels() / multiple' => [['id'=>4,'name'=>'Delta', 'hotels'=>[1,2,3]], 'hotels', $this->createCollectionMock($hotelCollection, [1,2,3]), NULL],
            'Invalid Data' => [['wubba'=>'lubba','dub'=>'dub'], NULL, NULL, 'InvalidArgumentException'],
            'Invalid Data -> id()' => [['id'=>1,'name'=>'Alpha'], 'id', 1, 'DomainException'],
        ];
    }

    /**
     * Checks that the class can initialize from given data and can retrieve it using getter method
     *
     * @param array $dataSet
     * @param string $getMethodName
     * @param mixed $expectedValue
     * @param string $expectedExceptionName
     *
     * @depends isTestable
     * @dataProvider dataSourceProvider
     * @test
     */
    public function canInitialize(array $dataSet, ?string $getMethodName, $expectedValue, ?string $expectedExceptionName)
    {
        if (!empty($expectedExceptionName)) $this->expectException($expectedExceptionName);

        $hotelCollection = $this->createMock(HotelCollection::class);
        $hotelCollectionMock = $this->createCollectionMock($hotelCollection);
        $hotelRegistry = $this->createMock(HotelRegistry::class);

        if (empty($expectedExceptionName))
            $hotelRegistry->expects($this->atLeastOnce())->method('getById')->willReturn($hotelCollectionMock);

        $instance = $this->class->newInstanceArgs([(object) $dataSet, $hotelRegistry]);

        if (!empty($expectedValue))
        {
            $value = $instance->$getMethodName();

            if (!($value instanceof HotelCollection)) $this->assertSame($expectedValue, $value);
            else {
                // Since I don't have time to properly mock a collection I wont
                // but it's not a world end, since the collection itself has tests
                // @TODO Properly mock a Collection and test the result
                $this->assertTrue(true);
            }
        }
        // If there is no expected output, then skip (probably it's waiting for an exception)
        else $this->markTestSkipped();
    }
}