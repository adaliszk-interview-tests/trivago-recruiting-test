<?php
/**
 * Copyright (C) 2017 Ádám Liszkai <adaliszk@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/environment.php';

// Instantiate the container
$container = new Illuminate\Container\Container();

$container->bind(
    AdaLiszk\Trivago\Recruiting\Boundaries\FileDataSource::class,
    AdaLiszk\Trivago\Recruiting\DataSources\JSONData::class
);

// Define CityRegistry class to it's interface
$container->bind(
    AdaLiszk\Trivago\Recruiting\Boundaries\Registries\City::class,
    AdaLiszk\Trivago\Recruiting\Cities\Registry::class
);
$container->bind(
    AdaLiszk\Trivago\Recruiting\Boundaries\Collections\Cities::class,
    AdaLiszk\Trivago\Recruiting\Cities\Collection::class
);
$container->bind(
    AdaLiszk\Trivago\Recruiting\Boundaries\Entities\City::class,
    AdaLiszk\Trivago\Recruiting\Cities\City::class
);

// Define HotelRegistry class to it's interface
$container->bind(
    AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Hotel::class,
    AdaLiszk\Trivago\Recruiting\Hotels\Registry::class
);

// Define PartnerRegistry class to it's interface
$container->bind(
    AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Partner::class,
    AdaLiszk\Trivago\Recruiting\Partners\Registry::class
);

// Define PriceRegistry class to it's interface
$container->bind(
    AdaLiszk\Trivago\Recruiting\Boundaries\Registries\Price::class,
    AdaLiszk\Trivago\Recruiting\Prices\Registry::class
);

return $container;